#include <stdio.h>

int main(void) {
    int a;
    printf("Enter data: ");
    scanf("%d", &a);

    int flip_num = 0;
    while (a > 0) {

        flip_num *= 10;
        flip_num += a % 10;
        a /= 10;

    }
    printf("%d", flip_num);

    return 0;
}