#include <stdio.h>

int main(void) {
    int a;
    scanf("%d", &a);

    int result = 1;
    while (a > 0) {
        if ((a % 10) % 2 != 0) {
            result = 0;
            break;
        }
        a /= 10;

    }
    printf(result ? "YES" : "NO");

    return 0;
}