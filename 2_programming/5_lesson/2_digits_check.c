#include <stdio.h>

int main(void) {
    int a;
    printf("Enter any number: ");
    scanf("%d", &a);
    int number_digits = 0;

    while (a > 0) {
        a /= 10;
        number_digits += 1;
    }

    //printf("number_digits = %d", number_digits);
    printf(number_digits == 3 ? "YES" : "NO");

    return 0;
}