#include <stdio.h>

int main(void) {
    int a, b;
    printf("Enter interval from a to b: ");
    scanf("%d %d", &a, &b);

    long int sum = 0;
    for (; a <= b; a++) sum += a * a;

    printf("Sum of squares from a to b = %d", sum);

    return 0;
}