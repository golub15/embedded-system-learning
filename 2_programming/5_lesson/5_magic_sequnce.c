#include <stdio.h>

int main(void) {
    int a;
    printf("Enter data: ");
    scanf("%d", &a);


    for (int i = 10; i <= a; i++) {
        int n = i, sum = 0, mult = 1;
        while (n > 0) {
            sum += n % 10;
            mult *= n % 10;
            n /= 10;
        }
        if (mult == sum) printf("%d ", i);
    }


    return 0;
}