#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*
YEAR;MONTH;DAY;HOUR;MINUTE;TEMPERATURE
dddd;mm;dd;hh;mm;temperature
dddd - год 4 цифры
mm - месяц 2 цифры
dd - день 2 цифры
hh - часы 2 цифры
mm - минуты 2 цифры
temperature - целое число от -99 до 99
*/

//Время и дата
typedef struct {
    uint16_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
} timedate;

// Метка температуры
typedef struct {
    timedate time;
    int8_t temperature;
} sensor;

// Статистика по температуре
typedef struct {
    int8_t temp_min; // Минимальное
    int8_t temp_max; // Максимальное
    float temp_average; // Среднее 
    uint64_t stamps_count; // Кол-во данных в выборке

    int64_t field;
} sensor_stats;

//Хранение истории
typedef struct {
    sensor* buffer; // Укаазатель на динам. память
    uint64_t size; // Размер
    size_t capacity; // Емкость для расширения
} sensor_history;



void ParseFromCSV(FILE* fs, sensor_history* data); //Чтение/проверка CSV и заполнеие данными
void SortByTimedateQuick(sensor_history* data); // Сортиповака по дате
uint64_t convertTimedateNumber(timedate time); // Конвертация в число (timestamp)

void InitSensorHistory(sensor_history* arr); // Иницализация массива (нач. выделение памяти с емкостью = 1)
void DeleteSensorHistory(sensor_history* arr); // Освобождение всей памяти (free)
void AddSensorStamp(sensor_history* arr, sensor element); // Добавление и расширение емкости (realloc)

void PrintSensorArray(sensor_history* data); // Печать меток
//Супер функция: статы за 12 месяцев и год. стата
void GetYearTempStats(sensor_history* data, sensor_stats* month_stats, sensor_stats* year_stats);
void PrintMonthStats(sensor_stats* month_stats, uint8_t month); // Печать за месяцы или месяц
void PrintYearStats(sensor_stats* year_stats); // За год
