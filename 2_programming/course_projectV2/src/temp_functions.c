#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "temp_functions.h"

typedef char byte;


void ParseFromCSV(FILE* fs, sensor_history* data) {
    char str[100];
    int file_line = 0;
    while (fgets(str, 100, fs)) {
        file_line++;
        int is_correct = 1;
        sensor tmp;

        int param_count = 0;
        int last_sep = 0;
        for (size_t i = 0; i < strlen(str); i++) {
            if (str[i] == ';' || i == 0) {
                int dec;
                if (i != 0) last_sep = i + 1;
                if (sscanf(str + last_sep, "%d", &dec)) {
                    //printf("%d\n", dec);

                    if (param_count == 0)tmp.time.year = dec;
                    else if (param_count == 1)tmp.time.month = dec;
                    else if (param_count == 2)tmp.time.day = dec;
                    else if (param_count == 3)tmp.time.hour = dec;
                    else if (param_count == 4)tmp.time.minute = dec;
                    else if (param_count == 5)tmp.temperature = dec;

                }
                else {
                    printf("Error parse numbers at line %d\n", file_line);
                    printf("> %s\n\n", str);
                    is_correct = 0;
                    break;
                }
                param_count++;
            }
        }
        if (param_count != 6 && is_correct) {
            printf("Error parse format at line %d\n", file_line);
            printf("> %s\n\n", str);
            is_correct = 0;
        }

        if (is_correct) {
            AddSensorStamp(data, tmp);
        }
    }
}

uint64_t convertTimedateNumber(timedate time) {
    uint64_t result = 0;

    result |= (uint64_t)time.year << 32;
    result |= (uint64_t)time.month << 28;
    result |= (uint64_t)time.day << 23;
    result |= (uint64_t)time.hour << 18;
    result |= (uint64_t)time.minute;

    return result;
}

int CompareDatetime(const void* x, const void* y) {
    sensor* a = (sensor*)x;
    sensor* b = (sensor*)y;
    return convertTimedateNumber(a->time) > convertTimedateNumber(b->time);
}

void SortByTimedateQuick(sensor_history* data) {
    qsort(data->buffer, data->size, sizeof(sensor), CompareDatetime);
}


void InitSensorHistory(sensor_history* arr) {
    arr->size = 0;
    arr->capacity = 1;
    arr->buffer = (sensor*)malloc(arr->capacity * sizeof(sensor));
}

void DeleteSensorHistory(sensor_history* arr) {
    arr->size = 0;
    arr->capacity = 0;
    free(arr->buffer);
}


void AddSensorStamp(sensor_history* arr, sensor element) {
    if (arr->size >= arr->capacity) {
        // Увеличение размера массива вдвое, если не хватает места
        arr->capacity *= 2;
        arr->buffer = (sensor*)realloc(arr->buffer, arr->capacity * sizeof(sensor));
    }
    arr->buffer[arr->size] = element;
    arr->size++;
}

void PrintSensorArray(sensor_history* data) {
    printf("============== Start ===================\n");
    for (uint64_t i = 0;i < data->size;i++)
        printf("%04d-%02d-%02d t=%3d\n",
        data->buffer[i].time.year,
        data->buffer[i].time.month,
        data->buffer[i].time.day,
        data->buffer[i].temperature
        );
    printf("=============== End ====================\n");

}

void PrintStatRow(sensor_stats* stat, int64_t first_value) {
    if (stat->stamps_count) {
        printf("%-7lld %-15lld %-7d %-7d %-4.1f\n",
            first_value,
            stat->stamps_count,
            stat->temp_min,
            stat->temp_max,
            stat->temp_average);
    }
    else {
        //printf("%-6d    -------------NO DATA----------------\n", first_value);
    }
}


void PrintYearStats(sensor_stats* year_stats) {
    printf("================= Year stats ====================\n");
    printf("Year    StampsCount     MinT    MaxT    AvrT\n");
    PrintStatRow(year_stats, year_stats->field);
}


void PrintMonthStats(sensor_stats* month_stats, uint8_t month) {
    /*
    Если month !=0 то печать за month
    */
    printf("================= Months stats ====================\n");
    printf("Month   StampsCount     MinT    MaxT    AvrT\n");
    if (month) { // Выввод только опр. месяца
        PrintStatRow(&month_stats[month], month);
        return;
    }

    for (int i = 0; i < 12; i++) {
        PrintStatRow(&month_stats[i], i + 1);
    }
}

// Возращает min/max/average для месяцев и года
// Принимает массив month_stats с 12 месяцами
void GetYearTempStats(sensor_history* data, sensor_stats* month_stats, sensor_stats* year_stats) {

    sensor_stats tmp = {
     .temp_max = INT8_MIN,
     .temp_min = INT8_MAX,
     .temp_average = 0.0f,
     .stamps_count = 0,
     .field = 0
    }; // Начальные данные

    for (int i = 0; i < 12; i++) {
        month_stats[i] = tmp;
    }
    *year_stats = tmp;

    for (uint64_t i = 0; i < data->size; i++) {
        if (0 < data->buffer[i].time.month && data->buffer[i].time.month <= 12) {

            uint8_t month = data->buffer[i].time.month;
            uint8_t month_index = month - 1;


            month_stats[month_index].stamps_count += 1;
            month_stats[month_index].temp_average += (float)data->buffer[i].temperature;


            if (data->buffer[i].temperature < month_stats[month_index].temp_min) {
                month_stats[month_index].temp_min = data->buffer[i].temperature;
            }

            if (data->buffer[i].temperature > month_stats[month_index].temp_max) {
                month_stats[month_index].temp_max = data->buffer[i].temperature;
            }
        }
    }

    // Расчет среденего для каждого месяца и статы для года
    for (int i = 0; i < 12; i++) {
        if (month_stats[i].stamps_count > 0) {

            month_stats[i].temp_average /= month_stats[i].stamps_count;

            year_stats->temp_average += month_stats[i].temp_average;
            year_stats->stamps_count += 1;

            if (month_stats[i].temp_min < year_stats->temp_min) {
                year_stats->temp_min = month_stats[i].temp_min;
            }

            if (month_stats[i].temp_max > year_stats->temp_max) {
                year_stats->temp_max = month_stats[i].temp_max;
            }
        }
    }

    if (year_stats->stamps_count > 0) {
        year_stats->temp_average /= year_stats->stamps_count;
        year_stats->field = data->buffer[0].time.year;
    }

}

