#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "temp_functions.h"

#define DEFAULT_FILE_NAME "temperature_small.csv"
#define PATH_LENGTH 200

sensor_history my_sensor_data;


int main(int argc, char* argv[]) {

    char dir[PATH_LENGTH] = DEFAULT_FILE_NAME;
    int rez = 0;
    uint8_t month = 0;
    uint8_t print_history = 0;
    //  opterr=0;
    while ((rez = getopt(argc, argv, "f:m:hp")) != -1) {
        switch (rez) {
        case 'h':
            printf("This app get .csv file\n");
            printf("Usage: clear [options]\n\
        -h This help text\n\
        -f Analysis .csv file with temperature.\n\
        -m View stats only for a given month.\n\
        -p Print sesnsor history");
            return 0;
        case 'f':
            strcpy(dir, optarg);
            break;
        case 'p':
            print_history = 1;
            break;
        case 'm':
            if (!sscanf(optarg, "%hhd", &month)) {
                printf("Invalid argument for month format %s\n", optarg);
                return 1;
            }
            else {
                if (month > 12 || month < 1) {
                    printf("Month must be in range 1-12 \n");
                    return 1;
                }
            }
            break;
            // printf("found argument \"C = %s\".\n", optarg); break;
        case '?':
            printf("Unknown argument: %s Try -h for help\n", argv[optind - 1]);
            return 1;

        };
    };

    FILE* fs;
    if ((fs = fopen(dir, "r")) == NULL) {
        perror("Error occured while opening input file !");
        return 1;
    }

    InitSensorHistory(&my_sensor_data);
    ParseFromCSV(fs, &my_sensor_data); // Чтение файла CSV

    sensor example_stamp;

    // Проверка динам. выделения памяти
    for (size_t i = 0; i < 12123; i++) {
        example_stamp.temperature = -45;
        timedate td = {
            .year = 2021,
            .month = 2,
            .day = i,
            .hour = i,
            .minute = i
        };
        example_stamp.time = td;

        AddSensorStamp(&my_sensor_data, example_stamp); // Добавление в конец
    }

    SortByTimedateQuick(&my_sensor_data); // Сортинг по температуре

    if (print_history)PrintSensorArray(&my_sensor_data);
    sensor_stats months_stats[12];
    sensor_stats year_stat;

    GetYearTempStats(&my_sensor_data, &months_stats[0], &year_stat);
    PrintYearStats(&year_stat);
    printf("\n");
    PrintMonthStats(&months_stats[0], month);
    DeleteSensorHistory(&my_sensor_data);

    return 0;
}