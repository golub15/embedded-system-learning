#include <stdio.h>

#define MATRIX_SIZE 5


int InputMatrix(int n, int m, int arr[m][n]);
int MatrixTrace(int n, int m, int arr[m][n]);
int main(void) {
    int matrix[MATRIX_SIZE][MATRIX_SIZE];
    InputMatrix(MATRIX_SIZE, MATRIX_SIZE, matrix);
    printf("trace= %d", MatrixTrace(MATRIX_SIZE, MATRIX_SIZE, matrix));

    return 0;
}



int MatrixTrace(int n, int m, int arr[m][n]) {
    int sum = 0;
    for (int i = 0; i < n;i++) {
        for (int j = 0; j < m;j++) {
            if (i == j) {
                sum += arr[i][j];
                //printf("%d", arr[i][j]);
            }
        }
    }
    return sum;
}


int InputMatrix(int n, int m, int arr[m][n]) {
    for (int i = 0; i < n;i++) {
        for (int j = 0; j < m;j++) {
            scanf("%d", &arr[i][j]);
        }
    }
}

