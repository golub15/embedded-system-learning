#include <stdio.h>

#define BUFFER_SIZE 200

int buffer[BUFFER_SIZE];

int FindMiss(int size, int a[]);

int InputGet(int arr[]); // до \n
void PrintArray(int* arr, int len);
int isDigit(char c);

int main(void) {
    printf("Enter data: ");
    int arr_len = InputGet(buffer);

    printf("Miss = %d", FindMiss(arr_len, buffer));
    return 0;
}

int isDigit(char c) // проверяем является ли символ цифрой
{
    return ((c >= '0') && (c <= '9'));
}


int InputGet(int arr[]) {
    int i=0;
    char c;
    while ((c = (char)getchar()) != '\n') {
        if (isDigit(c)) {
            int number = 0;
            do {
                number = number * 10 + c - '0';
                c = (char)getchar();
            } while (isDigit(c));

            if (number == 0) break;
            arr[i++] = number;
            if (c == '\n') break;
        }
    }
    return i;
}


void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int FindMiss(int size, int a[]) {
    int min_index = 0, max_index = 0, sum = 0;
    for (int i = 0; i < size;i++) {
        if (a[i] > a[max_index]) max_index = i;
        if (a[i] < a[min_index]) min_index = i;
        sum += a[i];
    }
    int sum_progression = ((a[max_index] + a[min_index]) * (a[max_index] - a[min_index] + 1)) / 2;
    // printf("p=%d\n", sum_progression);
    // printf("min=%d\n", a[min_index]);
    // printf("max=%d\n", a[max_index]);
    // printf("s=%d\n", sum);

    return sum_progression - sum;
}
