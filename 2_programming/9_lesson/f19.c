#include <stdio.h>
#include <stdlib.h>

#define MATRIX_SIZE 5


int InputMatrix(int n, int m, int arr[m][n]);
float MatrixDiagAvr(int n, int m, int arr[m][n]);
int Count(int n, int m, int arr[m][n], float avr);

int main(void) {
    int matrix[MATRIX_SIZE][MATRIX_SIZE];
    InputMatrix(MATRIX_SIZE, MATRIX_SIZE, matrix);
    float avr = MatrixDiagAvr(MATRIX_SIZE, MATRIX_SIZE, matrix);

    printf("count= %d", Count(MATRIX_SIZE, MATRIX_SIZE, matrix, avr));

    return 0;
}

int Count(int n, int m, int arr[m][n], float avr) {
    int c = 0;
    for (int i = 0; i < n;i++) {
        for (int j = 0; j < m;j++) {
            if (arr[i][j] > avr)c++;
        }
    }
    return c;
}

float MatrixDiagAvr(int n, int m, int arr[m][n]) {
    int sum = 0;
    for (int i = 0; i < n;i++) {
        for (int j = 0; j < m;j++) {
            if (i == j) {
                sum += abs(arr[i][j]);
            }
        }
    }
    return sum / m;
}


int InputMatrix(int n, int m, int arr[m][n]) {
    for (int i = 0; i < n;i++) {
        for (int j = 0; j < m;j++) {
            scanf("%d", &arr[i][j]);
        }
    }
}

