#include <stdio.h>

#define BUFFER_SIZE 200

int buffer[BUFFER_SIZE];

int is_two_same(int size, int a[]);

int InputGet(int arr[]); // до \n
void PrintArray(int* arr, int len);
int isDigit(char c);

int main(void) {
    printf("Enter data: ");
    int arr_len = InputGet(buffer);
    int res = is_two_same(arr_len, buffer);

    printf(res ? "YES" : "NO");
    return 0;
}

int isDigit(char c) // проверяем является ли символ цифрой
{
    return ((c >= '0') && (c <= '9'));
}


int InputGet(int arr[]) {
    int i=0;
    char c;
    while ((c = (char)getchar()) != '\n') {
        if (isDigit(c)) {
            int number = 0;
            do {
                number = number * 10 + c - '0';
                c = (char)getchar();
            } while (isDigit(c));
            arr[i++] = number;
            if (c == '\n')
                break;
        }
    }
    return i;
}


void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int is_two_same(int size, int a[]) {
    for (int i = 0; i < size;i++) {
        for (int j = i + 1; j < size;j++) {
            if (a[i] == a[j]) return 1;
        }
    }
    return 0;
}
