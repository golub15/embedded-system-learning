#include <stdio.h>

int main(void) {
    char litera = getchar();
    char digit = getchar();

    if ((digit - '1') % 2 == 0 && (litera - 'A') % 2 == 0) {
        printf("BLACK");
    }
    else {
        printf("WHITE");
    }

    return 0;
}