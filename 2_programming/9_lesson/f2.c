#include <stdio.h>

#define BUFFER_SIZE 200

int buffer[BUFFER_SIZE];

void sort_even_odd(int n, int a[]);

int InputArray(int arr[], int n); // Нужен терминирующий символ
int InputGet(int arr[]); // до \n
int isDigit(char c);
void PrintArray(int* arr, int len);

int main(void) {
    int arr_len = InputGet(buffer);
    

    sort_even_odd(arr_len, buffer);
    PrintArray(buffer, arr_len);

    return 0;
}

int isDigit(char c) // проверяем является ли символ цифрой
{
    return ((c >= '0') && (c <= '9'));
}


int InputGet(int arr[]) {
    int i=0;
    char c;
    while ((c = (char)getchar()) != '\n') {
        if (isDigit(c)) {
            int number = 0;
            do {
                number = number * 10 + c - '0';
                c = (char)getchar();
            } while (isDigit(c));
            arr[i++] = number;
            if (c == '\n')
                break;
        }
    }
    return i;
}



int InputArray(int arr[], int n) { 
    int i, num;
    while (scanf("%d", &num) != 0 && i < n - 1) {

        arr[i++] = num;
    }
    return i;
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}


void sort_even_odd(int n, int a[]) {
    int odd_tmp[n];
    int last_even_index = 0;
    int last_odd_index = 0;
    for (int i = 0; i < n; i++) {
        if (a[i] % 2 == 0) {
            a[last_even_index++] = a[i];
        }
        else {
            odd_tmp[last_odd_index++] = a[i];
        }
    }
    for (int i = 0; i < last_odd_index; i++) {
        a[i + last_even_index] = odd_tmp[i];
    }
}
