#include <stdio.h>

void to_bin_str(int x) {

    int tmp = 1;
    while (x > 0) {
        tmp += x % 2;
        tmp *= 10;
        x /= 2;
    }
    printf("%d\n", tmp);
}

void print_char_int(int x) {
    char* c;
    c = &x;

    to_bin_str(64);
    x += 1;
    printf("%d\n", x);

    printf("%c%c%c%c%c\n", *(c), *(c + 1), *(c + 2), *(c + 3), *(c + 4));
}

int main(void) {
    // int char_4;

    // char* c;
    // c = &char_4;

    // *(c) = 'b';
    // *(c + 1) = 'm';
    // *(c + 2) = 's';
    // *(c + 3) = 't';

    // *(c + 4) = 'u'; // no need


    // print_char_int(char_4);


    char str[100];
    *(str) = '1';
    *(str+1) = 'w';
    *(str+5) = '\0';
    printf("data: %s\n", &str);
    printf("data: %i\n", *(str+6));


    char *cst = "123456_1830";
    printf("%s", cst);
    
    return 0;
}