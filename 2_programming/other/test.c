#include <stdio.h>
int Input(int arr[], int n) {

    int i, num;
    while (scanf("%d", &num) != 0) {

        arr[i++] = num;
    }
    return i;
}


int isDigit(char c) // проверяем является ли символ цифрой
{
    return ((c >= '0') && (c <= '9'));
}


int InputGet(int arr[], int n) {
    int i;
    char c;
    while ((c = getchar()) != '\n') {
        if (isDigit(c)) {
            int number;
            do {
                number = number * 10 + c - '0';
                c = getchar();
            } while (isDigit(c));
            arr[i++] = number;
            if (c == '\n')
                break;
        }
    }
    return i;
}




// int InputGet(int arr[], int n) {
//     int i, num;
//     char c;
//     while ((c = getchar()) != '\n') {

//         arr[i++] = num;
//     }
//     return i;
// }


void BubbleSort(int arr[], int n) {
    int i = 0;
    int j = 0;
    int temp = 0;
    for (i = 0; i < n; ++i) {
        for (j = i; j < n; ++j) {
            if (arr[i] > arr[j]) {
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}
void Print(int arr[], int len) {
    for (int i = 0; i < len; i++)
        printf("%d ", arr[i]);
    printf("\n");
}
// int main() {
//     enum { BUFFER_SIZE = 5 };
//     int buffer[BUFFER_SIZE];
//     int len = Input(buffer, BUFFER_SIZE);
//     //BubbleSort(buffer, BUFFER_SIZE);
//     Print(buffer, len);
//     return 0;
// }

int main(void) {
    char str[] = "   -23;;;";
    int i;
    int r = sscanf(str,"%d", &i);
    printf("%d", r);
    return 0;
}

