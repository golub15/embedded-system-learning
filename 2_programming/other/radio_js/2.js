class RadioTest {
    static signalStrength = 100;
  
    static receive(message) {
      // Дополнительная логика обработки полученного сообщения
      return `Received message: ${message}`;
    }
  
    static transmit(message) {
      // Дополнительная логика передачи сообщения
      return `Transmitting message: ${message}`;
    }
  }
  
  // Пример использования класса
  const receivedMessage = RadioTest.receive("Hello, this is a test message.");
  console.log(receivedMessage);
  
  const transmittedMessage = RadioTest.transmit("This is a response message.");
  console.log(transmittedMessage);
  
  console.log(`Signal Strength: ${RadioTest.signalStrength}`);
  