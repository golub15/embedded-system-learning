
class RadioTest {
    // Статические свойства класса
    static exampleBuffer = [];
    static receivedIndex = 0;

    static ok_packets = 0;
    static cur_ok_packets = 0;

    // Пример буфера с данными


    // Моделируем функцию receive

    static receive(bytes) {
        const receivedData = this.exampleBuffer.slice(this.receivedIndex, this.receivedIndex + bytes);
        if (receivedData.length > 0) this.receivedIndex += bytes;
        return new Uint8Array(receivedData);
    }

    static generateRandomPacket() {
        // Генерация случайной длины от 3 до 254 байт
        const packetLength = Math.floor(Math.random() * (252 - 3 + 1)) + 3;

        // Генерация случайной последовательности байт
        const packetData = new Uint8Array(packetLength);
        for (let i = 0; i < packetLength; i++) {
            packetData[i] = Math.floor(Math.random() * 256);
        }


        return Array.from(packetData.map(item => (item === 126 ? 0 : item)));
    }


    // Моделируем функцию transmit
    static transmit(packet) {
        //console.log('Transmitting packet:', packet);
        this.cur_ok_packets++;
    }

    static putPacket(cor) {
        // Создаем массив, содержащий преамбулу (0x7e), метаданные, полезную нагрузку и постамбулу (0x7e)
        let payload = this.generateRandomPacket();

        // Вычисляем CRC-16/CCITT-FALSE для метаданных и полезной нагрузки
        let crc = calculateCRC(payload);

        // Добавляем вычисленную CRC в пакет (старший и младший байты)
        payload.push((crc >> 8) & 0xFF, crc & 0xFF);

        if (payload.includes(126)) {
            console.log('drop');
            return;
        }

        if (cor && payload[2] != 34) {
            payload[2] = 34;
        } else {
            this.ok_packets++;
        }
        //console.log('gen packet:', [0x7e, ...payload, 0x7e]);

        RadioTest.exampleBuffer = RadioTest.exampleBuffer.concat([0x7e, ...payload, 0x7e])

        return [0x7e, ...payload, 0x7e];

    }

    static putMock() {
        // Создаем массив, содержащий преамбулу (0x7e), метаданные, полезную нагрузку и постамбулу (0x7e)
        let payload = this.generateRandomPacket();
        RadioTest.exampleBuffer = RadioTest.exampleBuffer.concat(payload)
    }

}




'use strict';


var receiver
var transmitter



function setup() {
    let x = RadioTest
    receiver = x;
    transmitter = x;

}

var buffer = []; // Буфер для хранения данных пакета
var start = 0;

function loop() {
    //console.log('Test');


    while (true) {
        let receivedByte = receiver.receive(1);

        // console.log('raw: ', buffer);

        // не пустой
        if (receivedByte.length > 0) {

            if (receivedByte[0] === 126 && !start) {
                start = 1;
            }
            else if (receivedByte[0] === 126) {
                if (validatePacket(buffer)) {
                    // Если контрольная сумма верна, вызываем метод transmit
                    transmitter.transmit(new Uint8Array([126, ...buffer, 126]));
                }
                buffer = [];
            } else if (start) {
                buffer.push(receivedByte[0]); // Добавляем байт в буфер
            }
        } else {
            break;
        }
    }


}


// Функция для проверки контрольной суммы
function validatePacket(buffer) {
    // Проверяем, что буфер содержит как минимум метаданные, 1 байт, и контрольную сумму
    if (buffer.length < 5) {
        return false;
    }

    // Вычисляем CRC-16/CCITT-FALSE для метаданных и полезной нагрузки
    let calculatedCRC = calculateCRC(buffer.slice(0, buffer.length - 2));

    // Сравниваем вычисленную CRC с CRC в пакете
    let packetCRC = (buffer[buffer.length - 2] << 8) | buffer[buffer.length - 1];

    return calculatedCRC === packetCRC;
}

// Функция для вычисления CRC-16/CCITT-FALSE
function calculateCRC(data) {
    let crc = 0xFFFF;

    for (let i = 0; i < data.length; i++) {
        crc ^= data[i] << 8;

        for (let j = 0; j < 8; j++) {
            crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
        }
    }

    return crc & 0xFFFF;
}

setup()

let iter_test = 0

while (iter_test++ < 1000) {
    let t;
    if (Math.random() < 0.5) {
        t = RadioTest.putPacket(false);
    } else {
        t = RadioTest.putPacket(true)
    }

    if (Math.random() < 0.5) {
        RadioTest.putMock()
    }
    loop()

    if (RadioTest.ok_packets != RadioTest.cur_ok_packets) {
        console.log("Error", t);
        for (let i = 0; i < t.length; i++) {
            console.log(t[i]);
        }
        break;
    }
}




console.log(RadioTest.ok_packets);
console.log(RadioTest.cur_ok_packets);

