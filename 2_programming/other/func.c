#include <func.h>
#include <stdio.h>


void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}