//4 Задание
#include <stdio.h>
#include <stdlib.h>

double ffabs(double x) { if (x < 0) return x * -1.0f;else return x; }

int main(void) {
    double  find_sqrt = 10.0f;
    printf("x = %f \n", find_sqrt);
    double left, rigt = 0;
    rigt = find_sqrt;


    while (1) {
        double ans = (left + rigt) / 2;
        double sqr = ans * ans;
        //printf("%f", ans);
        printf("sqrt(x) = %f, %f, e=%f\n", ans, sqr, ffabs(find_sqrt - sqr));

        if (ffabs(find_sqrt - sqr) <= 0.00001f) {
            break;
        }
        else if (find_sqrt - sqr > 0) {
            left = ans;
        }
        else {
            rigt = ans;
        }
    }

    return 0;
}
