#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

int main(int argc, char *argv[]) {
    int mValue = -1; // Значение аргумента -m

    // Используем getopt для обработки аргументов командной строки
    int opt;
    while ((opt = getopt(argc, argv, "h::m:")) != -1) {
        switch (opt) {
            case 'm':
                mValue = atoi(optarg); // Преобразование аргумента -m в число
                break;
            case '?':
                if (optopt == 'm') {
                    fprintf(stderr, "Опция -%c требует аргумента.\n", optopt);
                } else {
                    fprintf(stderr, "Неизвестная опция '-%c'.\n", optopt);
                }
                return 1;
            default:
                fprintf(stderr, "Необработанная опция.\n");
                return 1;
        }
    }

    // Проверка наличия аргумента -m
    if (mValue == -1) {
        fprintf(stderr, "Необходимо указать значение для -m.\n");
        return 1;
    }

    // Ваш код для обработки значения mValue
    printf("ss -m: %d\n", mValue);

    return 0;
}
