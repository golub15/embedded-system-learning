//2 Задание
#include <stdio.h>
#include <stdlib.h>

// 9 hour (in lite) + 1.85 hour + 3.66 h (idle) + 1.375 hour (work) + 3.66 (idle)
// 54 (ostatok)        39 %

float tar(float cur_h, int silent) {

    if (silent) {
        if (cur_h >= 40 && cur_h < 59) return 100;
        else if (cur_h > 59) return 100;
    }
    else {
        if (cur_h >= 40 && cur_h < 59) return 50;
        else if (cur_h >= 60 && cur_h < 69) return 100;
        else if (cur_h >= 70 && cur_h < 79) return 150;
        else if (cur_h >= 80 && cur_h <= 100) return 200;
    }




}

int main(void) {
    float cur_hum = 90.0f;
    float full_power = 8.0f / 3600;
    float lite_power = 4.0f / 3600;
    float natur = 3.0f / 3600;
    int fan = 0;

    long long int seconds = 0;

    float rub = 0.0f;

    while (seconds <= 3600 * 24) {

        if (seconds % 3600 == 0) {
            printf("%f %d %lld\n", cur_hum, fan, seconds / 3600);
        }

        if (cur_hum >= 50 && !fan) {
            fan = 1;
        }
        if (fan) {
            //  printf("%f\n", ba.bki);


            if (cur_hum <= 39) fan = 0;

            if (seconds >= 3600 * 9 && seconds < 3600 * 22) {
                cur_hum -= full_power;
                rub += (200.0f / 3600);
            }
            else {
                cur_hum -= lite_power;
                rub += (100.0f / 3600);
            }
        }
        else {
            cur_hum += natur;
        }
        seconds++;

    }

    printf("%f\n", rub);
    //printf("%f\n", (float)fan_work_full / 3600.0f);

    return 0;
}
