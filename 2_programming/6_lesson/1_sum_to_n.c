#include <stdio.h>

long int sum_from_1_to_n(long int n); //  Прототип


int main(void) {
    int n;
    printf("Enter data: ");
    scanf("%d", &n);
    printf("%d", sum_from_1_to_n(n));

    return 0;
}
long int sum_from_1_to_n(long int n) {
    return (n * (1 + n) / 2);
}
