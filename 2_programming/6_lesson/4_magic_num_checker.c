#include <stdio.h>

int is_happy_number(int n);


int main(void) {
    int a;
    printf("Enter data: ");
    scanf("%d", &a);
    printf(is_happy_number(a) ? "YES" : "NO");
    return 0;
}

int is_happy_number(int n) {
    int  sum = 0, mult = 1;
    while (n > 0) {
        sum += n % 10;
        mult *= n % 10;
        n /= 10;
    }
    return mult == sum;
}
