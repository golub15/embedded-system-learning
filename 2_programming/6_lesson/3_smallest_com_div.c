#include <stdio.h>
#include <math.h>

int nod(int a, int b); //  Прототип


int main(void) {
    int a, b;
    printf("Enter data: ");
    scanf("%d %d", &a, &b);
    printf("%d", nod(a, b));

    return 0;
}
int nod(int a, int b) {
    while (a != 0 && b != 0) {
        if (a > b) a = a % b;
        else b = b % a;
    }
    return a + b;

}
