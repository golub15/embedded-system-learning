#include <stdio.h>

int main(void) {
    int a, b;
    printf("Enter any 2 numbers: ");
    scanf("%d", &a, &b);

    int max = a > b ? a : b;
    printf("max(%d, %d) = %d\n", a, b, max);

    return 0;
}