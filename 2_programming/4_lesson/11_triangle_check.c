#include <stdio.h>

int main(void) {
    int a, b, c;
    printf("Enter 3 any numbers: ");
    scanf("%d %d %d", &a, &b, &c);

    if (a < b + c && b < a + c && c < a + b) printf("YES");
    else printf("NO");

    return 0;
}