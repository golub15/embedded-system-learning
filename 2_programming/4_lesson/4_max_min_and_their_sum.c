#include <stdio.h>


int main(void) {
    int a, b, c, d, e;
    printf("Enter 5 any numbers: ");
    scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);

    int max = a, min = a;

    max = b > max ? b : max; min = b < min ? b : min;
    max = c > max ? c : max; min = c < min ? c : min;
    max = d > max ? d : max; min = d < min ? d : min;
    max = e > max ? e : max; min = e < min ? e : min;

    printf("max(%d, %d, %d, %d, %d) = %d\n", a, b, c, d, e, max);
    printf("min(%d, %d, %d, %d, %d) = %d\n", a, b, c, d, e, min);
    printf("min + max = %d\n", min + max);

    return 0;
}