#include <stdio.h>

int main(void) {
    int a;
    printf("Enter any month number: ");
    scanf("%d", &a);
    a = a / 12 > 0 ? 0 : a;

    switch (a / 3) {
    case 0:
        printf("winter\n");
        break;
    case 1:
        printf("spring\n");
        break;
    case 2:
        printf("summer\n");
        break;
    case 3:
        printf("autumn\n");
        break;

    default:
        break;
    }

    return 0;
}