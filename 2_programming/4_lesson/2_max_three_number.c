#include <stdio.h>

int main(void) {
    int a, b, c;
    printf("Enter 3 any numbers: ");
    scanf("%d %d %d", &a, &b, &c);
    
    int max = a > b ? a : b;
    printf("max(%d, %d, %d) = %d\n", a, b, c, c > max ? c : max);

    return 0;
}