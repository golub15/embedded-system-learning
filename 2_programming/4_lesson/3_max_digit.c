#include <stdio.h>

int main(void) {
    int a;
    printf("Enter any number: ");
    scanf("%d", &a);

    int a3 = a % 10;
    a /= 10;
    int a2 = a % 10;
    a /= 10;
    int a1 = a % 10;

    int max = a1 > a2 ? a1 : a2;
    printf("max(%d, %d, %d) = %d\n", a1, a2, a3, a3 > max ? a3 : max);

    return 0;
}