#include <stdio.h>

int main(void) {

    float x1, y1, x2, y2;
    printf("Enter 2 points over tab: ");
    scanf("%f %f %f %f", &x1, &y1, &x2, &y2);

    float tg = (y2 - y1) /(x2 - x1) ;

    printf("k = %.2f, b = %.2f\n", tg, (-tg * x1) + y1);


    return 0;
}