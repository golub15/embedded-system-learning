#include <stdio.h>

enum { BUFFER_SIZE = 10 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
void InversArray(int* arr, int begin, int end);
void Swap(int* a, int* b);



int main(void) {
    int arr[BUFFER_SIZE];
    printf("Enter data: ");
    InputArray(arr, BUFFER_SIZE);
    InversArray(arr, 0, 4);
    InversArray(arr, 5, 9);

    PrintArray(arr, BUFFER_SIZE);

    

    return 0;
}

void Swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void InversArray(int* arr, int begin, int end) {
    float sum = 0;
    for (; begin <= end;begin++, end--) {
        Swap(&arr[begin], &arr[end]);
    }
}

void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
