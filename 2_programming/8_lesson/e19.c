#include <stdio.h>

enum { BUFFER_SIZE = 30 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
void InversArray(int* arr, int begin, int end);
void Swap(int* a, int* b);

int NumToDigitsArray(int num, int* dst);

int main(void) {
    int dst[BUFFER_SIZE];
    printf("Enter data: ");
    int num;
    scanf("%d", &num);
    
    int len = NumToDigitsArray(num, dst);
    PrintArray(dst, len);
    return 0;
}

int NumToDigitsArray(int num, int* dst) {
    int dst_count = 0;
    while (num > 0) {
        dst[dst_count++] = num % 10;
        num /= 10;
    }
    InversArray(dst, 0, dst_count - 1);
    return dst_count;
}

void Swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void InversArray(int* arr, int begin, int end) {
    float sum = 0;
    for (; begin <= end;begin++, end--) {
        Swap(&arr[begin], &arr[end]);
    }
}

void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
