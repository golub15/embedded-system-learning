#include <stdio.h>

enum { BUFFER_SIZE = 30 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
void SortByDigitArray(int* arr, int len);
void Swap(int* a, int* b);

long long int ToMaxNum(long long int num);

int main(void) {
    printf("Enter data: ");
    long long int num;
    scanf("%lld", &num);

    long long int max_num = ToMaxNum(num);
    printf("Max num: %lld", max_num);
    return 0;
}

long long int ToMaxNum(long long int num) {
    int dst[60]; // buffer
    int dst_count = 0;
    while (num > 0) {
        dst[dst_count++] = num % 10;
        num /= 10;
    }

    SortByDigitArray(dst, dst_count);

    long long int res = 0;
    for (; dst_count >= 0;dst_count--) {
        res *= 10;
        res += dst[dst_count];
    }
    return res;
}

void SortByDigitArray(int* arr, int len) {
    int begin = 0, end = len;
    int min_index, max_index;


    for (;begin < end; begin++, end--) {

        min_index = begin; max_index = begin;
        for (int i = begin; i < end; i++) {
            if (arr[i] % 10 > arr[max_index] % 10) {
                max_index = i;
            }
            if (arr[i] % 10 < arr[min_index] % 10) {
                min_index = i;
            }
        }

        // Тут всегда свап
        Swap(&arr[begin], &arr[min_index]);

        //проверка на то что мин. не свап с макс. по индексу
        if (max_index != begin || min_index != end - 1) {
            // если на макс. знач свап мин.
            if (begin != max_index) {
                Swap(&arr[end - 1], &arr[max_index]);
            }
            else {
                Swap(&arr[end - 1], &arr[min_index]);
            }
        }
        //PrintArray(arr, BUFFER_SIZE);
    }

}


void Swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}


void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
