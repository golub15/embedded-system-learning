#include <stdio.h>

enum { BUFFER_SIZE = 5 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
float MiddleSequnce(int* arr, int len);



int main(void) {
    int arr[BUFFER_SIZE];
    printf("Enter data: ");
    InputArray(arr, BUFFER_SIZE);
    PrintArray(arr, BUFFER_SIZE);

    printf("%.3f", MiddleSequnce(arr, BUFFER_SIZE));

    return 0;
}

float MiddleSequnce(int* arr, int len) {
    float sum = 0;
    for (int i = 0; i < len;i++) {
        sum += (float)arr[i];
    }

    return sum / len;
}


void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
