#include <stdio.h>

enum { BUFFER_SIZE = 10 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
int FilterByDigitArray(int* src, int* dst, int len);


int main(void) {
    int src[BUFFER_SIZE];
    int dst[BUFFER_SIZE];
    printf("Enter data: ");
    InputArray(src, BUFFER_SIZE);

    int filt_arr_len = FilterByDigitArray(src, dst, BUFFER_SIZE);

    printf("Filtred data: ");
    PrintArray(dst, filt_arr_len);

    return 0;
}


int FilterByDigitArray(int* src, int* dst, int len) {
    int dst_ind = 0;
    for (int i = 0; i < len;i++) {
        int pre_last = (src[i] / 10) % 10;
        if (pre_last == 0) {
            dst[dst_ind++] = src[i];
        }
    }

    return dst_ind;
}

void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
