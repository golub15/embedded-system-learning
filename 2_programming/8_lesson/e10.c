#include <stdio.h>

enum { BUFFER_SIZE = 12 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
void RightShiftArray(int* arr, int len, int shift);



int main(void) {
    int arr[BUFFER_SIZE];
    printf("Enter data: ");
    InputArray(arr, BUFFER_SIZE);
    RightShiftArray(arr, BUFFER_SIZE, 4);
    PrintArray(arr, BUFFER_SIZE);

    return 0;
}

void RightShiftArray(int* arr, int len, int shift) {
    int tmp[shift];
    int tmp_i = 0;
    for (int i = len - 1; i >= 0;i--) {

        if (len - i <= shift) {
            tmp[tmp_i++] = arr[i];
        }
        if (i < shift) {
            arr[i] =  tmp[tmp_i - i - 1];
        }
        else {
            arr[i] = arr[i - shift];

        }
    }
}

void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
