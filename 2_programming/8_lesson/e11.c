#include <stdio.h>

enum { BUFFER_SIZE = 10 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
void SortByDigitArray(int* arr, int len);
void Swap(int* a, int* b);


int main(void) {
    int arr[BUFFER_SIZE];
    printf("Enter data: ");
    InputArray(arr, BUFFER_SIZE);
    SortByDigitArray(arr, BUFFER_SIZE);
    PrintArray(arr, BUFFER_SIZE);

    return 0;
}

void Swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}


void SortByDigitArray(int* arr, int len) {
    int begin = 0, end = len;
    int min_index, max_index;


    for (;begin < end; begin++, end--) {

        min_index = begin; max_index = begin;
        for (int i = begin; i < end; i++) {
            if (arr[i] % 10 > arr[max_index] % 10) {
                max_index = i;
            }
            if (arr[i] % 10 < arr[min_index] % 10) {
                min_index = i;
            }
        }

        // Тут всегда свап
        Swap(&arr[begin], &arr[min_index]);

        //проверка на то что мин. не свап с макс. по индексу
        if (max_index != begin || min_index != end - 1) {
            // если на макс. знач свап мин.
            if (begin != max_index) {
                Swap(&arr[end - 1], &arr[max_index]);
            }
            else {
                Swap(&arr[end - 1], &arr[min_index]);
            }
        }
        //PrintArray(arr, BUFFER_SIZE);
    }

}

void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
