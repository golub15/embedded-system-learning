#include <stdio.h>

enum { BUFFER_SIZE = 10 }; //указание количества элементов массива

void InputArray(int* arr, int len);
void PrintArray(int* arr, int len);
int TopNumberArray(int* src, int len);


int main(void) {
    int src[BUFFER_SIZE];
    printf("Enter data: ");
    InputArray(src, BUFFER_SIZE);

    int top_num = TopNumberArray(src, BUFFER_SIZE);

    printf("Top num: %d", top_num);

    return 0;
}


int TopNumberArray(int* src, int len) {
    int top_index = 0, top_count = 0, local_count = 0;
    for (int i = 0; i < len;i++) {
        local_count = 0;
        for (int j = i + 1; j < len;j++) {
            if (src[i] == src[j]) {
                local_count++;
            }
        }

        if (local_count > top_count) {
            top_count = local_count;
            top_index = i;
        }
    }

    return src[top_index];
}

void InputArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        scanf("%d", &arr[i]);
    }
}

void PrintArray(int* arr, int len) {
    for (int i = 0; i < len;i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
