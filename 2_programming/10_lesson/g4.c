#include <stdio.h>
#include <string.h>

#define FILE_NAME "2.txt"

int main(void) {
    FILE* fs;
    if ((fs = fopen(FILE_NAME, "r")) == NULL) {
        perror("Error occured while opening input file!");
        return 1;
    }

    char word[100];

    fscanf(fs, "%[^\n]", word);

    for (int i = 0; i < strlen(word);i++) {
        if (word[i] == '0') continue;
        if (word[i] == ' ') break;
        int in_word1 = 1, in_word2 = 0, flag=0;

        for (int j = i + 1; j < strlen(word);j++) {
            if (word[j] == ' ') flag = 1;
            if (word[i] == word[j]) {
                if(flag) in_word2++;
                else in_word1++;

                word[j] = '0';
            }
        }

        if (in_word1 == in_word2 && in_word1 == 1) {
            printf("%c ", word[i]);
        }
        word[i] = '0';
    }

    //printf("%s", word1);



    fclose(fs);
    return 0;
}