#include <stdio.h>
#include <string.h>

#define FILE_NAME_IN "2.txt"
#define FILE_NAME_OUT "2OUT.txt"

int main(void) {
    FILE* fs;
    if ((fs = fopen(FILE_NAME_IN, "r")) == NULL) {
        perror("Error occured while opening input file!");
        return 1;
    }

    char word[1000];
    char new_word[1000];
    int new_c = 0;
    fscanf(fs, "%[^\n]", word);


    for (int i = 0; i < strlen(word);i++) {
        if (word[i] == ' ') continue;
        new_word[new_c++] = word[i];
        for (int j = i + 1; j < strlen(word);j++) {
            if (word[i] == word[j]) {

                word[j] = ' ';
            }
        }

    }

    FILE* fs_out;
    fs_out = fopen(FILE_NAME_OUT, "w");
    fprintf(fs_out, new_word);

    fclose(fs_out);
    fclose(fs);
    return 0;
}