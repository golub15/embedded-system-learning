#include <stdio.h>
#include <string.h>

#define FILE_NAME_IN "2.txt"
#define FILE_NAME_OUT "2OUT.txt"

int main(void) {
    FILE* fs;
    if ((fs = fopen(FILE_NAME_IN, "r")) == NULL) {
        perror("Error occured while opening input file!");
        return 1;
    }

    char word[100];
    char long_word[100];
    int max_len = 0;
    while (fscanf(fs, "%s", word) != -1)
    {   
        if(strlen(word) > strlen(long_word)){
            strcpy(long_word, word);
        }
    }
    

    FILE* fs_out;
    fs_out = fopen(FILE_NAME_OUT, "w");
    fprintf(fs_out, long_word);

    fclose(fs_out);
    fclose(fs);
    return 0;
}