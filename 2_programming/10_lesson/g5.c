#include <stdio.h>
#include <string.h>

#define FILE_NAME_IN "2.txt"
#define FILE_NAME_OUT "2OUT.txt"

int main(void) {
    FILE* fs;
    if ((fs = fopen(FILE_NAME_IN, "r")) == NULL) {
        perror("Error occured while opening input file!");
        return 1;
    }

    char word[1000];
    fscanf(fs, "%[^\n]", word);

    for (int i = 0; i < strlen(word);i++) {

        switch (word[i]) {
        case 'a':
            word[i] = 'b';
            break;
        case 'b':
            word[i] = 'a';
            break;
        case 'A':
            word[i] = 'B';
            break;
        case 'B':
            word[i] = 'A';
            break;
        default:
            break;
        }


    }
    FILE* fs_out;
    fs_out = fopen(FILE_NAME_OUT, "w");
    fprintf(fs_out, word);

    fclose(fs_out);
    fclose(fs);
    return 0;
}