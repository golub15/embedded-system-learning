#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 200

#define FILE_NAME_IN "2.txt"
#define FILE_NAME_OUT "2OUT.txt"

int isDigit(char c);
int InputGetFromFile(FILE* fs, int arr[]);
void PrintArrayToFile(FILE* fs, int* arr, int len);
void Sort(int* arr, int len);
void Swap(int* a, int* b);


void Swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}


void Sort(int* arr, int len) {
    int begin = 0, end = len;
    int min_index, max_index;

    for (;begin < end; begin++, end--) {

        min_index = begin; max_index = begin;
        for (int i = begin; i < end; i++) {
            if (arr[i] > arr[max_index]) {
                max_index = i;
            }
            if (arr[i] < arr[min_index]) {
                min_index = i;
            }
        }

        // Тут всегда свап
        Swap(&arr[begin], &arr[min_index]);

        //проверка на то что мин. не свап с макс. по индексу
        if (max_index != begin || min_index != end - 1) {
            // если на макс. знач свап мин.
            if (begin != max_index) {
                Swap(&arr[end - 1], &arr[max_index]);
            }
            else {
                Swap(&arr[end - 1], &arr[min_index]);
            }
        }
        //PrintArray(arr, BUFFER_SIZE);
    }

}

int isDigit(char c) // проверяем является ли символ цифрой
{
    return ((c >= '0') && (c <= '9'));
}

void PrintArrayToFile(FILE* fs, int* arr, int len) {
    for (int i = 0; i < len;i++) {
        fprintf(fs, "%d ", arr[i]);
    }
}

int InputGetFromFile(FILE* fs, int arr[]) {
    int i = 0;

    signed char c;
    while ((c = (char)fgetc(fs)) != EOF) {
        if (isDigit(c)) {
            int number = 0;
            do {
                number = number * 10 + c - '0';
                c = (char)fgetc(fs);
            } while (isDigit(c));
            arr[i++] = number;

            if (c == EOF)
                break;
        }
    }
    return i;
}

int main(void) {
    FILE* fs;
    if ((fs = fopen(FILE_NAME_IN, "r")) == NULL) {
        perror("Error occured while opening input file!");
        return 1;
    }

    int arr[BUFFER_SIZE];
    int arr_len = InputGetFromFile(fs, arr);
    Sort(arr, arr_len);


    FILE* fs_out;
    fs_out = fopen(FILE_NAME_OUT, "w");

    PrintArrayToFile(fs_out, arr, arr_len);

    fclose(fs_out);
    fclose(fs);
    return 0;
}