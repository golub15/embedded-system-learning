#include <stdio.h>

void print_num(int num);


int main(void) {
    int n;
    printf("Enter data: ");
    scanf("%d", &n);
    print_num(n);

    return 0;
}

void print_num(int num) {
    if (num <= 0) return;

    print_num(num / 10);
    printf("%d ", num % 10);

}