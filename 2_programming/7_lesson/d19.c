#include <stdio.h>


int acounter(void);

int main(void){
    printf("%d", acounter());
    return 0;
}

int acounter(void){
    char c = getchar();
    if (c == '.') return 0;    
    if (c == 'a') return acounter() + 1;
    else return acounter();
}