#include <stdio.h>

void print_ab(int a, int b);


int main(void) {
    int a, b;
    printf("Enter data: ");
    scanf("%d %d", &a, &b);
    print_ab(a, b);

    return 0;
}

void print_ab(int a, int b) {
    printf("%d ", a);

    if (a == b) return;
    if (a < b) print_ab(a + 1, b);
    else print_ab(a - 1, b);
}