#include <stdio.h>

int recurs_power(int n, int p);

int main(void){
    int n, p;
    scanf("%d %d", &n, &p);
    printf("%d", recurs_power(n, p));

    return 0;
}

int recurs_power(int n, int p){
    if (p == 0) return 1;
    else if (--p)  return recurs_power(n, p) * n;
    else return n;
}