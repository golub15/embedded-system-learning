#include <stdio.h>


int akkerman(int n, int m);

int main(void) {
    int m, n;
    printf("Enter m n: ");
    scanf("%d %d", &m, &n);

    printf("%d", akkerman(m, n));

    return 0;
}

// int akkerman(int n, int m) {
//     printf("%d %d\n", n, m);
//     if (n == 0) return  m + 1;
//     else if (m == 0) return akkerman(n - 1, 1);
//     return akkerman(n - 1, akkerman(n, m - 1));
// }

int akkerman(int m, int n) {
    if (m == 0) return n + 1;
    else if (n == 0) return akkerman(m - 1, 1);
    else return akkerman(m - 1, akkerman(m, n - 1));
}