#include <stdio.h>


void func(void);

int main(void) {
    func();
    return 0;
}

void func() {
    int t;
    scanf("%d", &t);
    if (t % 2 != 0) printf("%d ", t);
    if (t == 0) return;
    func();

}