#include <stdio.h>

int count_bin(int num, int c);


int main(void) {
    int n;
    printf("Enter data: ");
    scanf("%d", &n);
    printf("%d", count_bin(n, 0));

    return 0;
}

int count_bin(int num, int c) {
    if (num <= 0) return c;
    if (num % 2) c+= 1;
    count_bin(num / 2, c);
}