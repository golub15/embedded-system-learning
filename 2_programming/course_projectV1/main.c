#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "temp_api.h"

#define SENSOR_ARRAY_SIZE 1000000
#define DEFAULT_FILE_NAME "temperature_small.csv"
#define PATH_LENGTH 200

sensor_history my_sensor_data;

int main(int argc, char* argv[]) {

    char dir[PATH_LENGTH] = DEFAULT_FILE_NAME;
    int rez = 0;
    int month = -1;
    //  opterr=0;
    while ((rez = getopt(argc, argv, "f:m:h")) != -1) {
        switch (rez) {
        case 'h':
            printf("This app get .csv file\n");
            printf("Usage: clear [options]\n\
        -h This help text\n\
        -f Analysis .csv file with temperature.\n\
        -m View stats only for a given month.\n");
            return 0;
        case 'f':
            strcpy(dir, optarg);
            break;
        case 'm':
            if (!sscanf(optarg, "%d", &month)) {
                printf("Invalid argument for month format %s\n", optarg);
                return 1;
            }
            else {
                if (month > 12 || month < 1) {
                    printf("Month must be in range 1-12 \n");
                    return 1;
                }
            }
            break;
            // printf("found argument \"C = %s\".\n", optarg); break;
        case '?':
            printf("Unknown argument: %s Try -h for help\n", argv[optind - 1]);
            return 1;

        };
    };

    FILE* fs;
    if ((fs = fopen(dir, "r")) == NULL) {

        perror("Error occured while opening input file !");
        return 1;
    }

    CreateSensorArray(&my_sensor_data, SENSOR_ARRAY_SIZE); // Динамическое выделение??? памяти на 1 миллион меток
    ParseFromCSV(fs, &my_sensor_data); // Чтение файла CSV
    DeleteSensorStamp(&my_sensor_data, 0); // Удаление 1 (или 0) элемента в массиве

    AddSensorStamp(&my_sensor_data, my_sensor_data.len, 2021, 6, 1, 1, 56, 95); // Добавление в конец
    AddSensorStamp(&my_sensor_data, 0, 2021, 12, 1, 1, 56, -95); // Добавление в начало

    SortByTemp(&my_sensor_data); // Сортинг по температуре
    PrintSensorArray(&my_sensor_data);

    if (month == -1) {
        for (int m = 1; m <= 12;m++) {
            PrintMonthStats(&my_sensor_data, m);
        }
    }
    else {
        PrintMonthStats(&my_sensor_data, month);
    }

    printf("=== Year statistics ===\n");
    printf("Min year temperature: %.1f\n", MinYearTemp(&my_sensor_data));
    printf("Max year temperature: %.1f\n", MaxYearTemp(&my_sensor_data));
    printf("Average year temperature: %.1f\n", AverageYearTemp(&my_sensor_data));

    return 0;
}