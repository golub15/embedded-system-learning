#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "temp_api.h"



void ParseFromCSV(FILE* fs, sensor_history* data) {
    char str[100];
    int data_len = 0;
    int file_line = 0;
    while (fgets(str, 100, fs)) {
        // printf("%s %d\n", str, file_line);

        file_line++;
        int is_correct = 1;
        sensor tmp;

        int param_count = 0;
        int last_sep = 0;
        for (int i = 0; i < strlen(str); i++) {
            if (str[i] == ';' || i == 0) {
                int dec;
                if (i != 0) last_sep = i + 1;
                if (sscanf(str + last_sep, "%d", &dec)) {
                    //printf("%d\n", dec);

                    if (param_count == 0)tmp.year = dec;
                    else if (param_count == 1)tmp.month = dec;
                    else if (param_count == 2)tmp.day = dec;
                    else if (param_count == 3)tmp.hour = dec;
                    else if (param_count == 4)tmp.minute = dec;
                    else if (param_count == 5)tmp.temperature = dec;

                }
                else {
                    printf("Error parse numbers at line %d\n", file_line);
                    printf("> %s\n\n", str);
                    is_correct = 0;
                    break;
                }
                param_count++;
            }
        }
        if (param_count != 6 && is_correct) {
            printf("Error parse format at line %d\n", file_line);
            printf("> %s\n\n", str);
            is_correct = 0;
        }

        if (is_correct) {
            data->info[data_len] = tmp;
            data_len++;
        }
    }

    data->len = data_len;

}

void SortByTemp(sensor_history* data) {
    int begin = 0, end = data->len;
    int min_index, max_index;

    for (;begin < end; begin++, end--) {

        min_index = begin; max_index = begin;
        for (int i = begin; i < end; i++) {
            if (data->info[i].temperature < data->info[max_index].temperature) {
                max_index = i;
            }
            if (data->info[i].temperature > data->info[min_index].temperature) {
                min_index = i;
            }
        }

        // Тут всегда свап
        cgangeIJ(data, begin, min_index);

        //проверка на то что мин. не свап с макс. по индексу
        if (max_index != begin || min_index != end - 1) {
            // если на макс. знач свап мин.
            if (begin != max_index) {
                cgangeIJ(data, end - 1, max_index);
            }
            else {
                cgangeIJ(data, end - 1, min_index);
            }
        }
        //PrintArray(arr, BUFFER_SIZE);
    }

}

void cgangeIJ(sensor_history* data, int i, int j) {
    sensor temp;
    temp = data->info[i];
    data->info[i] = data->info[j];
    data->info[j] = temp;
}


void CreateSensorArray(sensor_history* data, uint64_t len) {
    // Как я понял имеем динамическую память
    data->info = (sensor*)malloc(len * sizeof(sensor));
    if (data->info == NULL) {
        printf("Error malloc for sensor array.\n");
    }

    data->len = len;
}

void DeleteSensorStamp(sensor_history* data, uint64_t pos) {
    if (pos > data->len || !(data->len)) return; // Проверка на наличе

    for (int i = pos;i < data->len - 1;i++) {
        data->info[i] = data->info[i + 1];// сдвиг
    }
    data->len--;
}
void AddSensorStamp(sensor_history* data,
    uint64_t pos,
    uint16_t year,
    uint8_t month,
    uint8_t day,
    uint8_t hour,
    uint8_t minute,
    int8_t temperature) {

    data->len++;
    for (int i = data->len;i > pos;i--) {
        data->info[i] = data->info[i - 1];// сдвиг
    }

    data->info[pos].year = year;
    data->info[pos].month = month;
    data->info[pos].day = day;
    data->info[pos].hour = hour;
    data->info[pos].minute = minute;
    data->info[pos].temperature = temperature;
}

void PrintSensorArray(sensor_history* data) {
    printf("============== Start ===================\n");
    for (int i = 0;i < data->len;i++)
        printf("%04d-%02d-%02d t=%3d\n",
        data->info[i].year,
        data->info[i].month,
        data->info[i].day,
        data->info[i].temperature
        );
    printf("=============== End ====================\n");

}

void PrintMonthStats(sensor_history* data, uint8_t month) {
    printf("=== Month = %d ===\n", month);
    printf("Min temperature: %.1f\n", MinMonthTemp(data, month));
    printf("Max temperature: %.1f\n", MaxMonthTemp(data, month));
    printf("Average temperature: %.1f\n", AverageMonthTemp(data, month));
    printf("\n");
}

float AverageYearTemp(sensor_history* data) {
    float avr = 0;
    for (int i = 0; i < data->len; i++) {
        avr += (float)data->info[i].temperature / data->len;
    }

    return avr;
}

float MinYearTemp(sensor_history* data) {
    int8_t min_temp = 100;
    for (int i = 0; i < data->len; i++) {
        if (data->info[i].temperature < min_temp) min_temp = data->info[i].temperature;

    }
    return (float)min_temp;
}

float MaxYearTemp(sensor_history* data) {
    int8_t max_temp = -100;
    for (int i = 0; i < data->len; i++) {
        if (data->info[i].temperature > max_temp) max_temp = data->info[i].temperature;

    }
    return (float)max_temp;
}



float AverageMonthTemp(sensor_history* data, uint8_t month) {
    long long int sum = 0, count = 0;
    for (int i = 0; i < data->len; i++) {
        if (data->info[i].month == month) {
            sum += data->info[i].temperature;
            count++;
        }
    }

    return (float)sum / count;
}


float MinMonthTemp(sensor_history* data, uint8_t month) {
    int8_t min_temp = 100;
    for (int i = 0; i < data->len; i++) {
        if (data->info[i].month == month) {
            if (data->info[i].temperature < min_temp) min_temp = data->info[i].temperature;
        }
    }

    return (float)min_temp;
}

float MaxMonthTemp(sensor_history* data, uint8_t month) {
    int8_t max_temp = -100;
    for (int i = 0; i < data->len; i++) {
        if (data->info[i].month == month) {
            if (data->info[i].temperature > max_temp) max_temp = data->info[i].temperature;
        }
    }

    return (float)max_temp;
}
