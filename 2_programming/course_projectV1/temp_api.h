#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*
YEAR;MONTH;DAY;HOUR;MINUTE;TEMPERATURE
dddd;mm;dd;hh;mm;temperature
dddd - год 4 цифры
mm - месяц 2 цифры
dd - день 2 цифры
hh - часы 2 цифры
mm - минуты 2 цифры
temperature - целое число от -99 до 99
*/

typedef struct {
    uint16_t year;
    uint8_t month;
    uint8_t day;

    uint8_t hour;
    uint8_t minute;

    int8_t temperature;
} sensor;

typedef struct {
    sensor* info;
    uint64_t len;
} sensor_history;


void ParseFromCSV(FILE* fs, sensor_history* data);

void cgangeIJ(sensor_history* data, int i, int j);
void SortByTemp(sensor_history* data);

void CreateSensorArray(sensor_history* data, uint64_t len);
void DeleteSensorStamp(sensor_history* data, uint64_t pos);
void AddSensorStamp(sensor_history* data,
    uint64_t pos,
    uint16_t year,
    uint8_t month,
    uint8_t day,
    uint8_t hour,
    uint8_t minute,
    int8_t temperature);

void PrintSensorArray(sensor_history* data);
void PrintMonthStats(sensor_history* data, uint8_t month);

float AverageMonthTemp(sensor_history* data, uint8_t month);
float MaxMonthTemp(sensor_history* data, uint8_t month);
float MinMonthTemp(sensor_history* data, uint8_t month);

float AverageYearTemp(sensor_history* data);
float MaxYearTemp(sensor_history* data);
float MinYearTemp(sensor_history* data);