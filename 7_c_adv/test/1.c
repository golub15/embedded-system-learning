#include <stdio.h>
#include <inttypes.h>

uint16_t arr[] = {1, 2, 3};


void print_bin(uint8_t x)
{

    uint8_t c = 15;
    char str[17] = "0000000000000000\0";

    while (x > 0)
    {
        (x % 2) ? (str[c] = '1') : (str[c] = '0');
        x = x / 2;
        c -= 1;
    }

    printf("->");
    printf("%s", &str);
    printf("\n");
}

int main()
{

    uint32_t a = 15;
    uint32_t b = 0x56;

    // (a & b) ? printf("YES") : printf("NO");

    // printf("\n%d\n", a);

    uint32_t d = 0x00;

    d |= 0xaa;
    d = d << 8;
    d |= 0x55;

    uint8_t x = (d >> 8) & 0xff;

    print_bin(1);
    print_bin(2);
    print_bin(23 ^ 1 ^ 23);
    // print_bin((a << 8));
    // print_bin((a << 8) | (0xaa));

    // printf("OK\n");

    return 0;
}