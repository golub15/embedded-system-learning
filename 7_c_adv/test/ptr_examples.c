#include "stdio.h"
#include "stdlib.h"
#include "signal.h"

int irq = 0;



void sig_handler(int sig)
{
    irq = 1;
    printf("SIG: mem error!!\n");
    exit(-1);
}

void swap_val(int *val_a, int *val_b)
{
    int m = *val_a;
    *val_a = *val_b;
    *val_b = m;
}

void swap_ptr(int **ptr_a, int **ptr_b)
{
    int *m = *ptr_a;
    *ptr_a = *ptr_b;
    *ptr_b = m;
}

int main()
{
    int mock_var;
    signal(SIGSEGV, sig_handler);

    int x_val = 1;
    int y_val = 2;

    int *x = &x_val;
    int *y = &y_val;

    swap_ptr(&x, &y); // Меняет местами адреса указателей (их значения как УКАЗАТЕЛЬ)
    swap_val(x, y);   // Меняет местами значения по указателю

    printf("ptr_x= %d\n", *x);
    printf("ptr_y= %d\n", *y);

    printf("VAL_x= %d\n", *x);
    printf("VAL_y= %d\n", *y);

    return 0;
}
