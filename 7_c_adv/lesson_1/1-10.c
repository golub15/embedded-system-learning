#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>


struct pack_array
{
    uint32_t array;      // поле для хранения упакованного массива из 0 и 1
    uint32_t count0 : 8; // счетчик нулей в array
    uint32_t count1 : 8; // счетчик единиц в array
};

void array2struct(int[], struct pack_array *);

void array2struct(int bit_arr[], struct pack_array *pa)
{
    pa->array = 0;
    for (int i = 0; i < 32; i++)
    {
        pa->array |= (bit_arr[i] << (31 - i));

        if (bit_arr[i])
            pa->count1++;
        else
            pa->count0++;
    }
}

int main()
{
    int bit_arr[32];
    struct pack_array pa;

    for (int i = 0; i < 32; i++)
    {
        scanf("%d", &bit_arr[i]);
    }

    array2struct(bit_arr, &pa);

    printf("array= %x\n", pa.array);
    printf("count0= %u\n", pa.count0);
    printf("count1= %u\n", pa.count1);
    return 0;
}