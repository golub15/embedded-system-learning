#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main()
{
    uint32_t n, k;
    scanf("%u %u", &n, &k);
    uint32_t bit_mask = (0x01 << k) - 1;
    uint32_t max_val = 0;

    while (n > 0)
    {
        uint32_t mask_val = n & bit_mask;
        if (mask_val > max_val)
            max_val = mask_val;

        n = n >> 1; // также как n = n / 2;
        //printf("%u", n);
    }

    printf("%u", max_val);
    return 0;
}