#include <stdio.h>
#include <inttypes.h>


int main (){

    uint32_t n, k;
    printf("-> ");
    scanf("%u %u", &n, &k);
    printf("%uo",  n & ((1 << k ) - 1));

    return 0;
}