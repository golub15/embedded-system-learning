#include <stdio.h>
#include <inttypes.h>


int main (){
    uint32_t n, k;
    printf("-> ");
    scanf("%d %d", &n, &k);

    printf("%d", (n << (32 - k)) | (n >> k));

    printf("\n");

 
    return 0;
}