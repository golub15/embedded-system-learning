#include <stdio.h>
#include <inttypes.h>

int main()
{
    uint32_t N;
    char c;
    scanf("%u", &N);

    scanf("%c", &c); // пропускаем лишний символ

    while (1)
    {

        scanf("%c", &c);
        if (c == '.')
        {
            printf("%c", c);
            break;
        }

        if (c >= 'a')
        {
            c = (c - 'a') + N;
            c = c % 26;
            c = c + 'a';
        }
        else
        {
            c = (c - 'A') + N;
            c = c % 26;
            c = c + 'A';
        }

        printf("%c", c);
    }
    printf("\n");

    return 0;
}