#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int main()
{
    uint32_t n, arr_val, result = 0;
    scanf("%u", &n);

    while (n--)
    {
        scanf("%u", &arr_val);
        result = result ^ arr_val;
    }

    printf("%u", result);
    return 0;
}