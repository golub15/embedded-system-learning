#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>


int main()
{
    uint32_t n;
    scanf("%u", &n);
    uint32_t bit_n = 0;

    while (n > 0)
    {
        if(n & 0x01) bit_n++;
        n = n >> 1; // также как n = n / 2;
    }

    printf("%u", bit_n);
    return 0;
}