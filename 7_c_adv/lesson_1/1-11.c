#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int extractExp(float);

int extractExp(float val)
{
    union
    {
        struct
        {
            uint32_t mant : 23;
            uint32_t exp : 8;
            uint32_t sign : 1;
        } data;

        float val;
    } float_union;
    float_union.val = val;

    return float_union.data.exp;
}

int main()
{
    float in_val;
    scanf("%f", &in_val);

    printf("%u", extractExp(in_val));
    return 0;
}