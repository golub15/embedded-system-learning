#include <stdio.h>
#include <inttypes.h>

int main()
{
    uint32_t n, max_val = 0, counter = 0, val;
    scanf("%u", &n);

    while (n--)
    {
        scanf("%u", &val);
        if (val == max_val)
        {
            counter++;
        }
        else if (val > max_val)
        {
            max_val = val;
            counter = 1;
        }
    }

    printf("%u\n", counter);

    return 0;
}