#include <stdio.h>
#include <inttypes.h>

typedef struct list
{
    void *address;
    size_t size;
    char comment[64];
    struct list *next;
} list;

void *findMaxBlock(list *head);

void *findMaxBlock(list *head)
{
    size_t max_size = 0;
    list *max_block = NULL;

    while (head != NULL)
    {
        if (head->size > max_size)
        {
            max_size = head->size;
            max_block = head;
        }
        head = head->next;
    }

    return (void *)max_block;
}

int main()
{
    list *head = NULL;

    list b1;
    b1.size = 4500;
    b1.next = head;
    head = &b1;

    list b2;
    b2.size = 100;
    b2.next = head;
    head = &b2;

    list b3;
    b3.size = 0;
    b3.next = head;
    head = &b3;

    // b1.next = head;

    list *max_b = findMaxBlock(head);

    printf("%u", max_b->size); // пропускаем лишний символ
    printf("\n");

    return 0;
}