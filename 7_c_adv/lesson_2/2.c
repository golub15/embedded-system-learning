#include <stdio.h>
#include <inttypes.h>

typedef struct list
{
    void *address;
    size_t size;
    char comment[64];
    struct list *next;
} list;

size_t totalMemoryUsage(list *head);

size_t totalMemoryUsage(list *head)
{

    if (head == NULL)
        return 0; // если вернуть NULL, то получим ошибку [-Wint-conversion]

    size_t mem_sum = 0;

    while (head != NULL)
    {
        mem_sum += head->size;
        head = head->next;
    }

    return mem_sum;
}

int main()
{
    list *head = NULL;

    list b1;
    b1.size = 4500;
    b1.next = head;
    head = &b1;

    list b2;
    b2.size = 100;
    b2.next = head;
    head = &b2;

    list b3;
    b3.size = 20;
    b3.next = head;
    head = &b3;

    // b1.next = head;

    size_t tm = totalMemoryUsage(head);

    printf("%u", tm); // пропускаем лишний символ
    printf("\n");

    return 0;
}