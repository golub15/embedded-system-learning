#include "game.h"

struct control_buttons default_controls = {KEY_DOWN, KEY_UP, KEY_LEFT, KEY_RIGHT};

void initTail(struct tail_t t[], size_t size)
{
    struct tail_t init_t = {0, 0};
    for (size_t i = 0; i < size; i++)
    {
        t[i] = init_t;
    }
}
void initHead(struct snake_t *head, int x, int y)
{
    head->x = x;
    head->y = y;
    head->direction = RIGHT;
}

void initSnake(snake_t *head, size_t size, int x, int y)
{
    tail_t *tail = (tail_t *)malloc(MAX_TAIL_SIZE * sizeof(tail_t));
    initTail(tail, MAX_TAIL_SIZE);
    initHead(head, x, y);
    head->tail = tail; // прикрепляем к голове хвост
    head->tsize = size + 1;
    head->controls = default_controls;
}

void printLine(int x_start, int y_start, int x_end, int y_end)
{
    int x_len = (x_end - x_start);
    int y_len = (y_end - y_start);

    if (x_len > y_len)
    {
        int x_s = x_start;
        for (; x_start <= x_end; x_start++)
        {
            float m = (float)y_len / x_len;
            m *= (x_start - x_s);

            mvprintw(y_start + m, x_start, "%c", '#');
        }
    }
    else
    {
        int y_s = y_start;
        for (; y_start <= y_end; y_start++)
        {
            float m = (float)x_len / y_len;
            m *= (y_start - y_s);

            mvprintw(y_start, x_start + m, "%c", '#');
        }
    }
}

void DrawWalls()
{
    // Линии границы экрана
    int max_x = 0, max_y = 0;
    getmaxyx(stdscr, max_y, max_x); // macro - размер терминала
    printLine(0, 0, max_x - 1, 0);
    printLine(0, 0, 0, max_y - 1);
    printLine(max_x - 1, 0, max_x - 1, max_y - 1);
    printLine(0, max_y - 1, max_x - 1, max_y - 1);
}

/*
 Движение головы с учетом текущего направления движения
 */
void go(struct snake_t *head)
{

    // static int phase = 0, pd = 1;
    // static int prev_p = 0;
    // if (pd)
    // {
    //     if (head->direction == LEFT || head->direction == RIGHT)
    //         ++(head->y);
    //     else
    //         ++(head->x);

    //     phase++;
    // }
    // else
    // {
    //     if (head->direction == LEFT || head->direction == RIGHT)
    //         --(head->y);
    //     else
    //         --(head->x);
    //     phase--;
    // }

    // if (phase >= 1 || phase <= 0)
    // {
    //     pd = !pd;
    // }
    // prev_p = phase;

    char ch = '@';
    int max_x = 0, max_y = 0;
    getmaxyx(stdscr, max_y, max_x); // macro - размер терминала

    uint8_t new_game = 0;

    // Выход за границы стены
    if (head->x >= max_x - 1 || head->x <= 0 || head->y >= max_y - 1 || head->y <= 0)
    {
        new_game = 1;
    }

    // проверка на само-съедание хвоста
    for (size_t i = head->tsize - 1; i > 0; i--)
    {
        if (head->x == head->tail[i].x && head->y == head->tail[i].y)
            new_game = 1;
    }

    // Новая игра
    if (new_game)
    {
        // Очистка tail
        for (size_t i = head->tsize - 1; i > 0; i--)
            mvprintw(head->tail[i].y, head->tail[i].x, "%c", ' ');

        initSnake(head, 3, 10, 10);
    }

    mvprintw(head->y, head->x, " "); // очищаем один 2символ

    // Выход за экран

    // mvprintw(max_y - 1, max_x - 1, "%c", '%');

    switch (head->direction)
    {
    case LEFT:
        if (head->x <= 0) // Циклическое движение, чтобы не
                          // уходить за пределы экрана
            head->x = max_x;
        mvprintw(head->y, --(head->x), "%c", ch);
        break;
    case RIGHT:
        mvprintw(head->y, ++(head->x), "%c", ch);
        break;
    case UP:
        mvprintw(--(head->y), head->x, "%c", ch);
        break;
    case DOWN:
        mvprintw(++(head->y), head->x, "%c", ch);
        break;
    default:
        break;
    }
    // refresh();
}

void changeDirection(struct snake_t *snake, const int32_t key)
{
    if (key == snake->controls.down)
        snake->direction = DOWN;
    else if (key == snake->controls.up)
        snake->direction = UP;
    else if (key == snake->controls.right)
        snake->direction = RIGHT;
    else if (key == snake->controls.left)
        snake->direction = LEFT;
}

/*
 Движение хвоста с учетом движения головы
 */
void goTail(struct snake_t *head)
{
    char ch = '*';

    mvprintw(head->tail[head->tsize - 1].y, head->tail[head->tsize - 1].x, " ");
    for (size_t i = head->tsize - 1; i > 0; i--)
    {
        head->tail[i] = head->tail[i - 1];
        if (head->tail[i].y || head->tail[i].x)
        {
            mvprintw(head->tail[i].y, head->tail[i].x, "%c", ch);
        }
    }
    head->tail[0].x = head->x;
    head->tail[0].y = head->y;
}