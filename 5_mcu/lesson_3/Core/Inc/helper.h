/*
 * helper.h
 *
 *  Created on: May 31, 2024
 *      Author: ipigeon15
 */

#ifndef INC_HELPER_H_
#define INC_HELPER_H_

#include "main.h"



typedef struct __ScreenData{
	uint8_t counter;
	uint8_t updateFlag;
	uint32_t btn_timer;
}ScreenData;

void IntToStr(uint8_t number, char *strBuffer);
void UpadateScreen();

#endif /* INC_HELPER_H_ */
