/*
 * helper.c
 *
 *  Created on: May 31, 2024
 *      Author: ipigeon15
 */

#include "helper.h"

#include "st7789.h"
#include "stdio.h"
#include "7_segments.h"

ScreenData menu;
char strBuffer[30];

void IntToStr(uint8_t number, char *buf) {
	uint8_t len = 10;
	buf[len] = '\0';
	while (number > 0) {
		buf[--len] = (number % 10) + 48; // 48 = 0 char ascii
		number /= 10;
	}

	while(len > 0){
	    buf[--len] = '0';
	}

}

void UpadateScreen() {
	if (menu.updateFlag) {
		ST7789_print(0, 20, RGB565(255, 0, 0), RGB565(0, 10, 100), 1, &Font_11x18, 1, strBuffer);
		DisplayValue(menu.counter);

		//IntToStr(menu.counter, strBuffer);
		//ST7789_print(0, 0, RGB565(255, 0, 0), RGB565(0, 10, 100), 1, &Font_16x26, 1, strBuffer);

		//ST7789_print(x, y, TextColor, BgColor, TransparentBg, Font, multiplier, str)
		menu.updateFlag = 0;
	}
}

