/*
 * 7_segments.c
 *
 *  Created on: Jun 5, 2024
 *      Author: ipigeon15
 */

#include "7_segments.h"

const uint8_t SG_Digits[] = { 0b11111100, // 0
		0b01100000, // 1
		0b11011010, // 2
		0b11110010, // 3
		0b01100110, // 4
		0b10110110, // 5
		0b10111110, // 6
		0b11100000, // 7
		0b11111110, // 8
		0b11110110, // 9
		};

#define CHAR_E 0b00111010

int8_t display_value[3];
uint8_t blink_mode = 0;

void TransmitSerialData(uint8_t value) {
	for (uint8_t i = 0; i < 8; i++) {
		HAL_GPIO_WritePin(DS_PORT, DS_PIN, value & (1 << i));
		HAL_GPIO_WritePin(CLK_PORT, CLK_PIN, 1);
		HAL_GPIO_WritePin(CLK_PORT, CLK_PIN, 0);
	}

	HAL_GPIO_WritePin(LATCH_PORT, LATCH_PIN, 1);
	HAL_GPIO_WritePin(LATCH_PORT, LATCH_PIN, 0);
}

void Update7Indicators() {

	static uint8_t digit_phase = 0;
	static uint32_t phase_time = 0;
	static uint32_t blink_time = 0;

	static uint8_t blink_phase = 0;

	if (blink_mode & (HAL_GetTick() > blink_time)) {
		blink_phase = !blink_phase;
		blink_time = HAL_GetTick() + 500;
	}

	if (HAL_GetTick() > phase_time) {

#ifdef SG_1_GPIO_Port
		HAL_GPIO_WritePin(DIGIT_3_PORT, DIGIT_3_PIN, RESET);
		HAL_GPIO_WritePin(DIGIT_2_PORT, DIGIT_2_PIN, RESET);
		HAL_GPIO_WritePin(DIGIT_1_PORT, DIGIT_1_PIN, RESET);


		if (blink_phase)
			return;

		switch (digit_phase) {
		case 0:
			HAL_GPIO_WritePin(DIGIT_3_PORT, DIGIT_3_PIN, SET);
			TransmitSerialData(display_value[2]);
			break;
		case 1:
			HAL_GPIO_WritePin(DIGIT_2_PORT, DIGIT_2_PIN, SET);
			TransmitSerialData(display_value[1]);
			break;
		case 2:
			HAL_GPIO_WritePin(DIGIT_1_PORT, DIGIT_1_PIN, SET);
			TransmitSerialData(display_value[0]);
			break;
		default:
			break;
		}

#endif
		digit_phase++;

		if (digit_phase > 2)
			digit_phase = 0;
		phase_time = HAL_GetTick() + 1;
	}

}

void DisplayForEach(char char_1, char char_2, char char_3) {
	display_value[0] = char_1;
	display_value[1] = char_2;
	display_value[2] = char_3;
}

void DisplayValue(uint16_t value) {
	display_value[2] = SG_Digits[value % 10];
	display_value[1] = SG_Digits[(value / 10) % 10];
	display_value[0] = SG_Digits[(value / 100) % 10] | 0b00000001;
}
