/*
 * 7_segments.h
 *
 *  Created on: Jun 5, 2024
 *      Author: ipigeon15
 */

#ifndef INC_7_SEGMENTS_H_
#define INC_7_SEGMENTS_H_

#include "main.h"

// Указываем свои порты и пины


#define SG_O 0b00111010
#define SG_E 0b00111010
#define SG_DISABLE 0b00000000


#define CLK_PIN SG_CLOCK_Pin
#define CLK_PORT SG_CLOCK_GPIO_Port

#define DS_PIN SG_DS_Pin
#define DS_PORT SG_CLOCK_GPIO_Port

#define LATCH_PIN SG_LATCH_Pin
#define LATCH_PORT SG_LATCH_GPIO_Port

#ifdef SG_1_GPIO_Port

#define DIGIT_1_PIN SG_1_Pin
#define DIGIT_1_PORT SG_1_GPIO_Port

#define DIGIT_2_PIN SG_2_Pin
#define DIGIT_2_PORT SG_2_GPIO_Port

#define DIGIT_3_PIN SG_3_Pin
#define DIGIT_3_PORT SG_3_GPIO_Port

#endif


void TransmitSerialData(uint8_t value);
void DisplayValue(uint16_t value);
void Update7Indicators();
void DisplayForEach(char char_1, char char_2, char char_3);

#endif /* INC_7_SEGMENTS_H_ */
